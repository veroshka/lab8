﻿#include <iostream>
#include <fstream>


int main()
{
	
	int n, m;
	std::cin >> n >> m;
	
	int* mas = new int[m];

	int** matrix = new int* [n];
	for (int i = 0; i < n; i++)
		matrix[i] = new int[m];
		
	for (int i = 0; i < n; i++)
		for (int k = 0; k < m; k++)
		{
			std::cin >> matrix[i][k];
		}

	for (int i = 0; i < m; i++)
		mas[i] = 0;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			mas[i] = mas[i] + matrix[i][j];
	

	int min = INT_MAX;
	int n_stroki;
	for (int k = 0; k < n; k++)
		if (min > mas[k])
		{
			min = mas[k];
			n_stroki = k;
		}

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			if (i == n_stroki)
				matrix[i][j] = 0;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			std::cout << matrix[i][j] << " ";
		std::cout << std::endl;
	}

	for (int i = 0; i < n; i++)
		delete[] matrix[i];
	delete[] matrix;

	delete[] mas;

}
