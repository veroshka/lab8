﻿#include <iostream>
#include <string>

int main()
{
	int n;
	std::cin >> n;

	int* mas = new int[n];
	int* first = new int[n];
	int* sum = new int[n];
	

	for (int i = 0; i < n; i++)
	{
		std::cin >> mas[i];

		sum[i] = 0;
		int x = mas[i];
		while (x > 0)
		{
			sum[i] += x % 10;
			x = x / 10;
		}

		std::string s = std::to_string(mas[i]);
		first[i] = int(s[0]);
	}

	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
		{
			if (first[i] >= first[j] ||
				(first[i] == first[j] && sum[i] >= sum[j]) ||
				(first[i] == first[j] && sum[i] == sum[j] && mas[i] >= mas[j]))
			{
				std::swap(mas[i], mas[j]);
				std::swap(first[i], first[j]);
				std::swap(sum[i], sum[j]);
			}
		}
	for (int i = 0; i < n; i++)
		std::cout << "mas[" << i << "]=" << mas[i] << " " << std::endl;
	
	delete[] mas;
	delete[] first;
	delete[] sum;

}